#|
  This file is a part of todobackend-ningle project.
|#

(in-package :cl-user)
(defpackage todobackend-ningle-asd
  (:use :cl :asdf))
(in-package :todobackend-ningle-asd)

(defsystem todobackend-ningle
  :version "0.1"
  :author ""
  :license ""
  :depends-on (:ningle
               :clack
               ;; :for
               :jonathan)
  :components ((:module "src"
                :components
                ((:file "todobackend-ningle"))))
  :description ""
  :long-description
  #.(with-open-file (stream (merge-pathnames
                             #p"README.md"
                             (or *load-pathname* *compile-file-pathname*))
                            :if-does-not-exist nil
                            :direction :input)
      (when stream
        (let ((seq (make-array (file-length stream)
                               :element-type 'character
                               :fill-pointer t)))
          (setf (fill-pointer seq) (read-sequence seq stream))
          seq)))
  :in-order-to ((test-op (test-op todobackend-ningle-test))))
