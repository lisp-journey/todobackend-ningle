(in-package :cl-user)
(defpackage todobackend-ningle
  (:use :cl
        :jonathan
        :ningle))
(in-package :todobackend-ningle)

;; (defun change-headers (headers)
;;   (setf (lack.response:response-headers *response*) headers))
;; ;; complains that it returns nil.

(defstruct task
  (title)
  (done))
;; => we read the slots with task-title and task-done.

(defvar *todos* '()
  "List of todos.")

(defun todos-to-plist (struct-list)
  "Get a list of todo structs and return a plist, for Jonathan."
  (mapcar (lambda (it)
            `(:title ,(task-title it)
              :done ,(task-done it)))
          struct-list))

;; ;; QL can not find "for"...
;; (defun to-plist (todos)
;;   "Return a plist of todos (same as with mapcar)."
;;   (let (plist)
;;     (for:for ((it over todos))
;;       (push `(:title ,(task-title it)
;;               :done ,(task-done it))
;;             plist))
;;     plist))

(defvar *app* (make-instance 'ningle:<app>))


(setf (ningle:route *app* "/")
      #'(lambda (params)
          (declare (ignore params))
          (setf (lack.response:response-headers *response*) '(:access-control-allow-origin "*"))
          (to-json (todos-to-plist *todos*))))
