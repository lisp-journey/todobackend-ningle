
;; Loads our app and stays up.
;; We could also use screen:
;; screen -d -m ros run --load run.lisp

(load "todobackend-ningle.asd")
;; (asdf:operate 'asdf:load-op 'todobackend-ningle)
(ql:quickload :todobackend-ningle)
;; ((lambda () todobackend-ningle:*app*)) ;; what for ?
(clack:clackup todobackend-ningle::*app* :port 9999)
;; (ql:quickload "swank")
;; (swank:create-server :port 4005 :dont-close t) ;; error: ASDF/INTERFACE::OPERATION-FORCED is undefined
