# lisp-todobackend

Todo-backend implementation in Common Lisp, with the Ningle microframework.

# Overview

http://www.todobackend.com/contribute.html

# Usage

The goal is to let this running in a server somewhere and to test
it against the todobackend test suite.

Run:

    make run

and access `localhost:9999`. This lets the app running in foreground.

possible:

    screen -d -m ros run --load run.lisp


# TODOs

- [X] Ningle project, create TODOs, return and display TODOs.
- [X] set up CORS
  - https://www.html5rocks.com/en/tutorials/cors/
  - I use emacs'
    [restclient.el](https://github.com/pashky/restclient.el) instead
    of curl or httpie to manually test requests. Pretty cool.
  - thanks Ealhad on SO https://stackoverflow.com/questions/46739404/how-to-enable-cors-in-hunchentoot-or-clack-or-how-to-add-a-specific-header
- [X] run from the shell with `make run`, see `run.lisp`.
  - [ ] start swank server ?
- [ ] test against the todobackend test suite
- [ ] implement what's needed.
- [ ] write tutorial.

# License

Copyright (c) 2017 vindarel

Licensed under the MIT License.
