#|
  This file is a part of todobackend-ningle project.
|#

(in-package :cl-user)
(defpackage todobackend-ningle-test-asd
  (:use :cl :asdf))
(in-package :todobackend-ningle-test-asd)

(defsystem todobackend-ningle-test
  :author ""
  :license ""
  :depends-on (:todobackend-ningle
               :prove)
  :components ((:module "t"
                :components
                ((:test-file "todobackend-ningle"))))
  :description "Test system for todobackend-ningle"

  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run-test-system) :prove-asdf) c)
                    (asdf:clear-system c)))
